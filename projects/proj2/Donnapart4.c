/*
* Description: Project2 - Part3
*              MCP Schedules Processes
*
* Author: Donna Hooshmand
*
* Date: 4/25/2020
*
* Notes:
* 1.
*/

/*-------------------------Preprocessor Directives---------------------------*/
#define _GNU_SOURCE
#define UNUSED __attribute__((unused))

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
/*---------------------------------------------------------------------------*/

/*-----------------------------Program Main----------------------------------*/

void signalHandler(int sig) {
  printf("Child process: %d - recieved signal: %d", getpid(), sig);
}

int printproc( pid_t pid){
    int intpid;
    const size_t pidsize = BUFSIZ;
    size_t bufsize = 256 * sizeof(char);
    char buf[BUFSIZ];
    //char line[BUFSIZ];
    char* empty = "";
    strcpy(buf, empty);
    DIR* proc = opendir("/proc");
    if(proc == NULL){
        perror("opening proc");
        return 1;
    }
    char* procdir = "/proc/";
    strcat(buf, procdir);
    //turn pid into int
    intpid = (int)pid;
    char strpid[BUFSIZ];
    snprintf(strpid, pidsize, "%d", intpid);
    strcat(buf, strpid);

    // //accessing /proc/[pid]/io
    // //this file contains I/O statistics for the process
    // printf("===================== I/O =====================\n");
    // char *io = "/io";
    // char bufio[BUFSIZ];
    // strcpy(bufio, buf);
    // strcat(bufio, io);
    // //printf("!!!!!!!!!!!!! IO FILE IS: %s\n", bufio);
    // FILE* FILEIO = fopen(bufio, "r");
    // if (FILEIO == NULL){
    //     perror("opening FILEIO");
    //     return 1;
    // }
    // while(fgets(line, BUFSIZ, FILEIO)){
    //     printf("%s",line);
    // }
    // printf("==================================================\n");
    // printf("===================== Status =====================\n");
    // char *status = "/status";
    // char bufstatus[BUFSIZ];
    // strcpy(bufstatus, buf);
    // strcat(bufstatus, status);

    // FILE* FILESTATUS = fopen(bufstatus, "r");
    // if(FILESTATUS == NULL){
    //     perror("opening FILESTATUS");
    //     return 1;
    // }
    // while(fgets(line, BUFSIZ, FILESTATUS)){
    //     printf("%s", line);
    // }
    // printf("================================================\n");
    // printf("===================== Stat =====================\n");
    
    char* stat = "/stat";
    char bufstat[BUFSIZ];
    strcpy(bufstat, buf);
    strcat(bufstat, stat);

    FILE* FILESTAT = fopen(bufstat, "r");
    if(FILESTAT == NULL){
        //perror("opening FILESTAT");
        return 1;
    }
    // while(fgets(line, BUFSIZ, FILESTAT)){
    //     printf("%s", line);
    //}
    char* line = (char *)malloc(sizeof(char) * 256); 
    getline(&line, &bufsize, FILESTAT);
    char seperator[2] = " ";
    ///#############Parse the data################
    //WHAT ARE THE FIELDS? 
    //(1) pid %d
            // The process ID.
    char* processID;
    processID = strtok(line, seperator);

    //(2) comm %s
            //The filename of the executable, in parenthesis.
            //This is visible whether or not the executable is swapped out.
    char* comm;
    comm = strtok(NULL, seperator);

    //(3) state %c
            //one of the following characters, indicating process state:
            // R  Running
            // S  Sleeping in an interruptible wait
            // D  Waiting in uninterruptible disk sleep
            // Z  Zombie
            // T  Stopped (on a signal) or (before Linux 2.6.33)
                    // trace stopped
            // t  Tracing stop (Linux 2.6.33 onward)
            // W  Paging (only before Linux 2.6.0)
            // X  Dead (from Linux 2.6.0 onward)
            // x  Dead (Linux 2.6.33 to 3.13 only)
            // K  Wakekill (Linux 2.6.33 to 3.13 only)
            // W  Waking (Linux 2.6.33 to 3.13 only)
            // P  Parked (Linux 3.9 to 3.13 only)
    char state;
    sscanf(strtok(NULL, seperator), "%c", &state);

    //(4) ppid %d
            //the PID of the parent of this process
    int ppid; 
    sscanf(strtok(NULL, seperator), "%d", &ppid);

    //(5) pgrp %d 
            //tbe process group ID of the process
    //I don't think this part is important and tbh idk what this means,
    //so I'm just gonna skip it.
    strtok(NULL, seperator);

    //(6) session %d
            // the session ID of the process.
    //skip. doesn't seem important.
    strtok(NULL, seperator);

    //(7) tty_nr %d
        // the controlling terminal of the process.
    //skip.
    strtok(NULL, seperator);

    //(8) tpgid %d
            // The ID of the foreground process group of the trolling terminal of the process
    //skip.
    strtok(NULL, seperator);

    //(9) flags %u
            // The kernel flags word of the process. 
    //skip.
    strtok(NULL, seperator);

    //(10) minflt %lu
            // the number of minor faults the process has made which have not required loading a memory
            // page from disk
    //skip.
    strtok(NULL, seperator);

    //(11) cminflt %lu
            // The number of minor faults that the process's waited-for children have made.
    //skip.
    strtok(NULL, seperator);

    //(12) majflt %lu 
            // The number of major faults the process has made which have required loading a memory
            // page from disk.
    //skip.
    strtok(NULL, seperator);

    //(13) cmajflt %lu
            // the number of major faults that the process's waited-for children have made.
    //skip.
    strtok(NULL, seperator);

    //(14) utime %lu
            // Amount of time that this process has been scheduled in user mode, measured in clock ticks.
    unsigned long utime;
    sscanf(strtok(NULL, seperator), "%lu", &utime);

    //(15) stime %lu
            // Amount of time that this process has been scheduled in kernel mode, measured in clock ticks.
    unsigned long stime;
    sscanf(strtok(NULL, seperator), "%lu", &stime);

    //(16) cutime %ld
            // Amount of time that this process's waited-for children have been scheduled in user mode
            // measured in clock ticks.
    //skip.
    strtok(NULL, seperator);

    //(17) cstime %ld
            // Amount of time that this process's waited-for children have been scheduled in kernel mode
            // measured in clock ticks.
    //skip.
    strtok(NULL, seperator);

    //(18) priority %ld
            // ...not much to explain. seems important. print it.
    long priority;
    sscanf(strtok(NULL, seperator), "%lu", &priority);

    //(19) nice %ld
            // The nice value, seems like another type of priority. print. 
    long nice;
    sscanf(strtok(NULL, seperator), "%lu", &nice);

    //(20) num_threads %ld
            // number of threads in this process.
    //skip.
    strtok(NULL, seperator);

    //(21) itrealvalue %ld
            // the time in jiffies before the next SIGALRM is sent to the process due to an interval timer.
    //skip.
    strtok(NULL, seperator);

    //(22) starttime %llu
            // the time the process started after system boot.
    unsigned long long starttime;
    sscanf(strtok(NULL, seperator), "%llu", &starttime);

    //(23) vsize %lu
            // virtual memory size in bytes.
    unsigned long vsize;
    sscanf(strtok(NULL, seperator), "%lu", &vsize);

    //all the other ones don't seem important 
    
    printf("%s\t%s\t%c\t%d\t%lu\t%lu\t%ld\t%ld\t%llu\t%lu\t\n",processID,comm,state,ppid,(utime/sysconf(_SC_CLK_TCK)), (stime/sysconf(_SC_CLK_TCK)),
        priority, nice, (starttime / sysconf(_SC_CLK_TCK)), vsize >> 10);
    // fclose(FILEIO);
    // fclose(FILESTATUS);
    free(line);
    fclose(FILESTAT);
    closedir(proc);
    return 0;
}



int main(UNUSED int argc, char* argv[]){
    //char *exit_code = "exit\n";
    char *input = NULL;
    size_t bufsize = 256 * sizeof(char);
    char *program, *token, *arguments[bufsize];
    //char *pid_list;
    pid_t pid_list[100];
    FILE* fp;
    int count = 0;
    int i = 0;
    
    pid_t current;
    pid_t ppid = getpid();
    pid_t pid;
    
    input = (char*)malloc(sizeof(char)*bufsize);
    if (input == NULL) {
        fprintf(stderr, "Unable to allocate memory \n");
        exit(EXIT_FAILURE);
    }
    
    fp = fopen(argv[1], "r");
    if(fp == NULL){
        fprintf(stderr, "Unable to open file: %s\n",argv[1]);
        exit(EXIT_FAILURE);
    }
    
    int status;
    sigset_t signalSet;
    sigemptyset(&signalSet);
    sigaddset(&signalSet, SIGALRM);
    //sigaddset()
    sigprocmask(SIG_BLOCK, &signalSet, NULL);
    // struct sigaction action;
    // memset(&action, '\0', sizeof(action));
    // action.sa_handler = &signalHandler;
    // action.sa_flags = SA_SIGINFO;

    //if(sigaction(SIGUSR1, &action, NULL) < 0){
    //    perror("sigaction error!");
    //    return 1;
    //}
    //sigaction(SIGUSR1, &action, NULL);
    
    while(getline(&input, &bufsize, fp) != EOF){
        i = 0;
        token = strtok(input, "\n ");
        if (token != NULL) {
            program = token;
            arguments[i] = token;
            i++;
            token = strtok(NULL, "\n ");
        }

        while (token != NULL) {
            arguments[i] = token;
            token = strtok(NULL, "\n ");
            i++;
        }
        arguments[i] = NULL;
        pid_list[count] = fork();
        
        if (pid_list[count] < 0) {
            printf("Error! Process: %d - Unable to create child process.\n", getpid());
            exit(EXIT_FAILURE);
        }
        if (pid_list[count] == 0) {
            //CHILD PROCESS
            raise(SIGSTOP);
            if (execvp(program, arguments) == -1){ perror("exec"); }      
 
            exit(1);
        }
        count++;
    }

    //printf("Parent process starting execution \n");
    //run until everything is finished
    // int condition = 1;
    int r = 0;  
    // int next = 1;
    // pid_t wpid;
    // int alive[count+1];
    // for(int s; s < count ; s++){
    //     alive[s] = 0;  //meaning process is alive
    // }

    //int done_childern;
    int escape = 0;
    int alive_children = 0;

    struct dirent* ent;
    long tgid;

    while (escape == 0){
        int done_childern = 0;
        // waitpid(-1, NULL, WNOHANG);
        system("clear");
        printf("===============================================================================================\n");
        printf("PID\tComm\t\tState\tPPID\tUserTime (s)\tKernelTime (s)\tPriority\tNice\nStartTime (s)\tVMSize (KB)\t\n");
        printf("-----------------------------------------------------------------------------------------------\n");
        
        if (kill(pid_list[r], 0) != -1) {
            kill(pid_list[r], SIGCONT);
            alarm(1);

            for(int h = 0; h < count; h++){
                if (kill(pid_list[h], 0) != -1) {
                    printproc(pid_list[h]);
                }
            }
            printf("===============================================================================================\n");
            int sig = SIGALRM;
            sigwait(&signalSet, &sig);
            if (kill(pid_list[r], 0) != -1) {
                kill(pid_list[r], SIGSTOP);
                
            }
            sleep(1);
         }
        r++;
        if(r > count-1){
            r = 0;
        }
        alive_children = 0;
        for(int e = 0; e < count; e++){
            waitpid(pid_list[e], &status, WNOHANG);
            if(WIFEXITED(status)){
                alive_children++;
            }
        }
        if(alive_children == count){
            escape = 1;
            break;
        }
    //printf("===============================================================================================\n");
    }
    
    for(int p = 0; p<count; p++){
        //waitpid(pid_list[j], &status,0);
        kill(pid_list[p], SIGCONT);
        waitpid(pid_list[p], NULL, 0);
    }
    printf("Done! all processes have finished! :) \n");
    free(input);
    fclose(fp);
    return 0;
}

/*-----------------------------Program End-----------------------------------*/

