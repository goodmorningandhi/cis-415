/* Project 2, part 3
 * Author: Liwei Fan
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>

#define CNUM 4

void sigHandler(int sig);
void signaler(pid_t *pid, int sig, int pidnum);
void command_handler(char* line);
void alaHandler(int signum);

int main(int argc, char** argv){
    /* =============== variables =================== */
    
    int pstatus, pcount = 0, count = 0;
    FILE* fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t nread;
    sigset_t set;
    sigemptyset(&set);
    sigaddset(&set, SIGUSR1);
    struct sigaction act;
    act.sa_handler = sigHandler;
    act.sa_mask = set;
    act.sa_flags = 0;
    sigaction(SIGUSR1, &act, NULL);

    /* =============== Load file =================== */

    if(argc != 2){
        fprintf(stderr, "%s: Invalid argument(s)\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    if((fp = fopen(argv[1], "r")) == NULL){
        fprintf(stderr, "%s: Error opening file: %s\n", argv[0], argv[1]);
        exit(EXIT_FAILURE);
    }

    /* =============== Count programs =================== */
    while((nread = getline(&line, &len, fp)) != -1){
        count++;
    }
    pid_t pid[count];
    /* =============== Load data ======================== */
    FILE* fp1 = fopen(argv[1], "r");
    size_t lenn = 0;
    ssize_t nnread;
    char* nline = NULL;
    for(int z = 0; z < count; z++){
        nnread = getline(&nline, &lenn, fp1);
        if (nnread == -1) break;
        pid[pcount] = fork();
        if(pid[pcount] < 0){
            fprintf(stderr, "Child process: %d - Fork failed!\n", getpid());
            exit(EXIT_FAILURE);
        }else if(pid[pcount] == 0){
            fprintf(stdout,"========== Child process: %d - Waiting for SIGUSR1 =======\n", getpid());
            fprintf(stdout,"--------------- My parent is %d --------------\n", getppid());
            int sig;
            sigwait(&set, &sig);
            fprintf(stdout, "Child process: %d - Received SIGUSR1 - Calling exec() \n", getpid());
            command_handler(nline);
            exit(EXIT_SUCCESS);
        }
        pcount++;
    }
    signaler(pid, SIGUSR1, count);
    signaler(pid, SIGSTOP, count);
    signaler(pid, SIGCONT, count);
    // for part 3, stop everything first, then make a scheduling
    signaler(pid, SIGSTOP, count); 
    //signal(SIGALRM, alaHandler);
    sigemptyset(&set);
    sigaddset(&set, SIGALRM);
    act.sa_handler = alaHandler;
    act.sa_mask = set;
    act.sa_flags = 0;
    sigaction(SIGALRM, &act, NULL);
    int fl = 1;
    int last = 0;
    //printf("here\n");
    sleep(2);
    while(fl){
        int cc = 0;
        for(int b = 0; b < count; b++){
            int status;
            int p = waitpid(pid[b], &status, WUNTRACED);
            
            if(WIFSTOPPED(status) != 0){
                //printf("pid: %d, status: %d, returned: %d\n", pid[b], status, p);
                // p > 0 means pid[b] still alive but stopped
                if(p > 0) kill(pid[b], SIGCONT);
                alarm(1);
                int sig;
                sigwait(&set, &sig);
                int sa;
                int q = waitpid(pid[b], &sa, WNOHANG);
                //printf("pid: %d, status: %d, returned: %d\n", pid[b], sa, q);
                if(q==0){
                    // Since WNOHANG set, q == 0 means pid[b] still alive.
                    kill(pid[b], SIGSTOP);
                }else{
                    // Another situation is pid[b] exiting during this loop.
                    printf("pid: %d exited\n", pid[b]);
                    cc++;
                }
                
            }
            if(WIFEXITED(status) != 0){
                // Evaluates to a non-zero value if status was returned for a child process that exited normally.
                fprintf(stdout, "pid: %d has been exited\n", pid[b]);
                cc++;
            }
            if(cc == count) {
                fl = 0;
                break;
            }
        }
        if(cc == (count-1)){
            last = 1;
            fl = 0;
            break;
        }
    }
    //printf("last: %d\n",last);
    int reco = 0;
    if(last == 1){
        for(int o = 0; o < count; o++){
            int fpid = waitpid(pid[o], &pstatus, WNOHANG);
            if(fpid == 0){
                fprintf(stdout, "Sending SIGCONT to the last running pid: %d \n",pid[o]);
                kill(pid[o], SIGCONT);
                reco = o;
            }
        }
    }
    
    wait(&pstatus);
    if(last == 1){
        fprintf(stdout, "pid: %d exited\n", pid[reco]);
    }
    fprintf(stdout, "All proceses exited! :)\n");
    free(line);
    fclose(fp);
    fclose(fp1);
    free(nline);
    return 0;
}

void alaHandler(int signum){

    fprintf(stdout, "Recived alarm signal stop 1 second\n");
}

void sigHandler(int sig){
    fprintf(stdout, "Child process: %d - Received signal: SIGUSR1\n", getpid());
}
void signaler(pid_t *pid, int sig, int pidnum){
    for(int j = 0; j < pidnum; j++){
        fprintf(stdout, "Parent process: %d - Sending signal: %d to child process: %d\n", getpid(), sig, pid[j]);
        kill(pid[j], sig);
    }
    sleep(1);
}

void command_handler(char* line){
    // Handler for commands "ls", "sleep", "./iobound" and "./cpubound"
    char* token = NULL;
    line[strcspn(line, "\n")] = '\0';
    // since no command w/ arguments more than 4.
    char* comarray[5];
    int comcount = 0;
    char* seveptr;
    //fprintf(stdout, "Handler running\n");
    // split a line of data into an array.
    while((token = strtok_r(line, " ", &seveptr)) != NULL){
        line = NULL;
        comarray[comcount] = malloc(sizeof(char)*(strlen(token)+1));
        strcpy(comarray[comcount], token);
        if((strcmp(token, "./iobound") == 0)||((strcmp(token, "./cpubound") == 0))){
            comcount++;
            comarray[comcount] = malloc(sizeof(char)*strlen(seveptr));
            strcpy(comarray[comcount], seveptr);
            comarray[comcount+1] = NULL;
            break;
        }
        comcount++;
    }

    if(strcmp(comarray[0], "sleep") == 0){
        comarray[comcount] = NULL;
        fprintf(stdout, "Now sleeping for %d seconds\n", atoi(comarray[1]));
        sleep(atoi(comarray[1]));
    }else if(strcmp(comarray[0], "ls") == 0){
        comarray[comcount] = NULL;
        fprintf(stdout, "Now showing ls\n");
        execvp(comarray[0], comarray);
    }else if(strcmp(comarray[0], "./iobound") == 0){
        fprintf(stdout, "Now running ./iobound\n");
        execvp(comarray[0], &comarray[1]);
    }else if(strcmp(comarray[0], "./cpubound") == 0){
        fprintf(stdout, "Now running ./cpubound\n");
        execvp(comarray[0], &comarray[1]);
    }else{
        fprintf(stderr, "Error: Invalid program\n");
    }

    for(int k = 0; k < comcount; k++){
        free(comarray[k]);
    }
}


