/* Project 2, part 1
 * Author: Liwei Fan
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>

#define CNUM 4

void command_handler(char* line);

int main(int argc, char** argv){
    /* =============== variables =================== */
    
    int status, pcount = 0, count = 0;
    FILE* fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t nread;

    /* =============== Load file =================== */

    if(argc != 2){
        fprintf(stderr, "%s: Invalid argument(s)\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    if((fp = fopen(argv[1], "r")) == NULL){
        fprintf(stderr, "%s: Error opening file: %s\n", argv[0], argv[1]);
        exit(EXIT_FAILURE);
    }

    /* =============== Count programs =================== */
    while((nread = getline(&line, &len, fp)) != -1){
        count++;
    }
    pid_t pid[count];
    /* =============== Load data ======================== */
    FILE* fp1 = fopen(argv[1], "r");
    size_t lenn = 0;
    ssize_t nnread;
    char* nline = NULL;
    for(int z = 0; z < count; z++){
        nnread = getline(&nline, &lenn, fp1);
        if (nnread == -1) break;
        pid[pcount] = fork();
        if(pid[pcount] < 0){
            fprintf(stderr, "Child process: %d - Fork failed!\n", getpid());
            exit(EXIT_FAILURE);
        }else if(pid[pcount] == 0){
            fprintf(stdout,"========== Child process: %d - running =======\n", getpid());
            fprintf(stdout,"--------------- My parent is %d --------------\n", getppid());
            command_handler(nline);
            exit(EXIT_SUCCESS);
        }else{
            wait(&status);
        }
        pcount++;
    }
    free(line);
    fclose(fp);
    fclose(fp1);
    free(nline);
    return 0;
}

void command_handler(char* line){
    // Handler for commands "ls", "sleep", "./iobound" and "./cpubound"
    char* token = NULL;
    line[strcspn(line, "\n")] = '\0';
    // since no command w/ arguments more than 4.
    char* comarray[5];
    int comcount = 0;
    char* seveptr;
    //fprintf(stdout, "Handler running\n");
    // split a line of data into an array.
    while((token = strtok_r(line, " ", &seveptr)) != NULL){
        line = NULL;
        comarray[comcount] = malloc(sizeof(char)*(strlen(token)+1));
        strcpy(comarray[comcount], token);
        if((strcmp(token, "./iobound") == 0)||((strcmp(token, "./cpubound") == 0))){
            comcount++;
            comarray[comcount] = malloc(sizeof(char)*strlen(seveptr));
            strcpy(comarray[comcount], seveptr);
            comarray[comcount+1] = NULL;
            break;
        }
        comcount++;
    }

    if(strcmp(comarray[0], "sleep") == 0){
        comarray[comcount] = NULL;
        fprintf(stdout, "Now sleeping for %d seconds\n", atoi(comarray[1]));
        sleep(atoi(comarray[1]));
    }else if(strcmp(comarray[0], "ls") == 0){
        //fprintf(stdout, "%d\n",comcount);
        comarray[comcount] = NULL;
        fprintf(stdout, "Now showing ls\n");
        execvp(comarray[0], comarray);
    }else if(strcmp(comarray[0], "./iobound") == 0){
        fprintf(stdout, "Now running ./iobound\n");
        execvp(comarray[0], &comarray[1]);
    }else if(strcmp(comarray[0], "./cpubound") == 0){
        fprintf(stdout, "Now running ./cpubound\n");
        execvp(comarray[0], &comarray[1]);
    }else{
        fprintf(stderr, "Error: Invalid program\n");
    }

    for(int k = 0; k < comcount; k++){
        free(comarray[k]);
    }
}