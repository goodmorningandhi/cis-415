==10071== Memcheck, a memory error detector
==10071== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==10071== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==10071== Command: ./a.out -f input.txt
==10071== 
==10071== 
==10071== HEAP SUMMARY:
==10071==     in use at exit: 0 bytes in 0 blocks
==10071==   total heap usage: 26 allocs, 26 frees, 399,192 bytes allocated
==10071== 
==10071== All heap blocks were freed -- no leaks are possible
==10071== 
==10071== For counts of detected and suppressed errors, rerun with: -v
==10071== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
