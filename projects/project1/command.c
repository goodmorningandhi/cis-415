#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include "command.h"
#define MAX_LEN 261

void listDir(){
    /*for the ls command*/
    char cwd[MAX_LEN];
    DIR* d;
    struct dirent *dptr;

    // get current working dir
    getcwd(cwd, sizeof(cwd));

    // open cwd
    d = opendir(cwd);
    do{
        if((dptr = readdir(d)) == NULL) break;
        write(1, (dptr->d_name), strlen(dptr->d_name));
        write(1, " ", 1);
    }while(1);
    closedir(d);
    free(dptr);
    write(1, "\n", 1);
}

void showCurrentDir(){
    /*for the pwd command*/
    char cwd[MAX_LEN];

    if(getcwd(cwd, sizeof(cwd))!=NULL){
        write(1, cwd, strlen(cwd));
        write(1, "\n", 1);
    }
}

void makeDir(char *dirName){
    /*for the mkdir command*/
    mkdir(dirName, 0777);
}

void changeDir(char *dirName){
    /*for the cd command*/
    chdir(dirName);
}

void copyFile(char *sourcePath, char *destinationPath){
    /*for the cp command*/
    int fd1, fd2, fd3;
    char cwd[MAX_LEN];
    char* desfile;
    // save current working dir.
    getcwd(cwd, sizeof(cwd));
    //printf("%s, %s", sourcePath, destinationPath);
    // read source file
    fd1 = open(sourcePath, O_RDONLY);
    char* buf = calloc(1024*128, sizeof(char));
    fd2 = read(fd1, buf, 128*1024);
    chdir(destinationPath);
    desfile = strrchr(sourcePath, '/');
    // then open dest. file.
        // +1 for eliminate '/'
    fd3 = open((desfile+1),(O_WRONLY|O_TRUNC|O_CREAT), 0777);
    fd3 = write(fd3, buf, fd2);
    close(fd3);
    close(fd1);
    chdir(cwd);
    free(buf);
}

void moveFile(char *sourcePath, char *destinationPath){
    /*for the mv command*/
    int fd1, fd2, fd3;
    char cwd[MAX_LEN];
    char* desfile;
    // save current working dir.
    getcwd(cwd, sizeof(cwd));

    fd1 = open(destinationPath,(O_WRONLY|O_TRUNC|O_CREAT), 0777);
    fd2 = open(sourcePath, O_RDONLY);
    char* buf = malloc(1024*128*sizeof(char));
    fd3 = read(fd2, buf, 1024*128);
    unlink(sourcePath);
    chdir(destinationPath);
    fd1 = write(fd1, buf, fd3);
    close(fd1);
    close(fd2);
    chdir(cwd);
    free(buf);
}

void deleteFile(char *filename){
    /*for the rm command*/
    if(unlink(filename) == -1){
        write(1, "Error! Unable rm the file: ", 27);
        write(1, filename, strlen(filename));
        write(1, "\n", 1);
    }
}

void displayFile(char *filename){
    /*for the cat command*/
    int fd1, fd2;
    char* line1 = calloc(1024,sizeof(char));
    if((fd1 = open(filename, O_RDONLY)) < 0){
        write(1, "Error! Unable display the file: ", 32);
        write(1, filename, strlen(filename));
        write(1, "\n", 1);
    }else{
        fd2=read(fd1, line1, 1024);

            write(1, line1, strlen(line1));


    }
    free(line1);
    close(fd1);
}
