/*
 * Description: CIS415 Spring:
 *              Project 1 main file.
 *			    Pseudo-Shell:
 *              A single-threaded UNIX system command-line interface.
 *
 * Author: Liwei Fan
 *
 * Date: 04/18/2020
 *
 * Notes:
 * 1. Included command.h.
 *
 * 2. Disabled buffering for stdout.
 *
 * 3. File descriptor:
 *    0 : stdin; 1 : stdout; 2 : stderr.
 *
 * 4. Remember that:
 *    4.1 ssize_t getline(char **lineptr, size_t *n, FILE *stream);        --> free lines
 *    4.2 char *strtok_r(char *str, const char *delim, char **saveptr);    --> set buff on str and each loop set buff to NULL
 *    4.3 ssize_t write(int fd, const void *buf, size_t count);            --> fd can be numbers(0,1,2), count can use strlen(buf)
 *    4.4 size_t strcspn(const char *s, const char *reject);               --> using the for find and eliminate '\n', etc.
 *    4.5 int strcmp(const char *s1, const char *s2);                      --> return 0 if equal
 *
 * 5. Support commands:
 *    Command     #args     Example             Desciption
 *    --------    -----     ------------        -----------------------------------------------------
 *    ls          0         ls                  Unix ls command. Will show the names of files and
 *                                              folders of the current directory.
 *
 *    pwd         0         pwd                 Unix pwd command. Will show the current directory.
 *
 *    mkdir       1         mkdir <name>        Unix mkdir command. Will create a directory with the
 *                                              filename specified by: <name>.
 *
 *    cd          1         cd directory        Unix cd command. Will change directory based on
 *                                              the destination specified in the argument.
 *
 *    cp          2         cp <src> <dst>      Only for copying files. Copy one file from the path
 *                                              specified by <src> to the location specified by <dst>.
 *
 *    mv          2         mv <src> <dst>      Only for moving files(cut-paste). Move a file from the
 *                                              the location specified by <src> to the location
 *                                              specified by <dst>.
 *
 *    rm          1         rm <filename>       Only for removing files (not a directory). Will remove
 *                                              the specified file from the current directory.
 *
 *    cat         1         cat <filename>      Displays a file's content on the console.
 */

/*===========================================================================*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "command.h"

/*===========================================================================*/

void error_handler(int x, char* commd);
int commands_handler(char* commands);

/*===========================================================================*/

void error_handler(int x, char* commd){
    /* error handler for wrong cases */
    switch (x){
        case 1: write(1, "Error! Unrecognized command: ", 29);
            write(1, commd, strlen(commd));
            write(1, "\n", 1);
            break;
        case 2: write(1, "Error! Incorrect syntax. No control code found.", 47);
            write(1, "\n", 1);
            break;
        case 3: write(1, "Error! Unsupported parameters for command: ", 43);
            write(1, commd, strlen(commd));
            write(1, "\n", 1);
            break;
        //case 4 for unexecpted option for ./pseudo-shell
        case 4: write(1, "Error! Unsupported mode for: ", 29);
            write(1, commd, strlen(commd));
            write(1, "\n", 1);
            break;
        // case 5 for NULL input file
        case 5: write(1, ": Invalid input file: ", 23);
            write(1, commd, strlen(commd));
            write(1, "\n", 1);
            break;
        // case 6 for too many input files
        case 6: write(1, ": Invalid input: too many files.", 23);
            write(1, "\n", 1);
            break;
        default:// do nothing on default
            break;
    }
}


int commands_handler(char* commands){

    /* handler for a set of commands
     * Return 1 for give up rest of line (because detected an error),
     *        0 for no errors and done for the line.
     * Noted: There are no ";" in 'commands' now.
     *        <keyword> <para> <para> <para> ...
     *        get keyword first
     *
     * #args: | ls    0 | pwd 0 | cp 2 | mv  2 |
     *        | mkdir 1 | cd  1 | rm 1 | cat 1 |
     */

    // variables
    char* stoken = NULL;
    char* sseveptr;
    char* charbuf = commands;
    // since no command accpect para. more than 3
    char** tbuf = malloc(sizeof(char*)*4);
    int giveup = 0;  // giveup flag
    int counter = 0; // counter for paras
    typedef enum{
        LS = 1, PWD,
        CP, MV,
        MKDIR, CD, RM, CAT
    } keyword;
    char* cmds[] = {"ls", "pwd", "cp", "mv", "mkdir", "cd", "rm", "cat"};
    keyword check = 0; // set & reset for next line

    // get keyword: check first.
    stoken = strtok_r(charbuf, " ", &sseveptr);
    charbuf = NULL;
    if(!check){
        for(int i = 0; i < 8; i++){
            if(strcmp(stoken, cmds[i]) == 0){
                check = i+1;
            }
        }
    }
    // loop for delimit the rest of line & count # of para.
    while(check){
        stoken = strtok_r(charbuf, " ", &sseveptr);
        charbuf = NULL;
        if(stoken == NULL){
            break;
        }
        // saving current token in the tbuf as 2d-array.
        for(int j = 0; j < 8; j++){
            if(strcmp(stoken, cmds[j]) == 0){
                // checking repeat command w/o control code
                error_handler(2, "repeated");
                giveup = 1;
                goto label1;
            }
        }
        tbuf[counter] = stoken;
        //printf("%d, %s\n", counter, stoken);
        counter++;
        if(counter == 3){
            // no command has 3 para, thus error!
            error_handler(3, cmds[check-1]);
            giveup = 1;
            goto label1;
        }
    }

    switch (check){
        case LS:
            // 0 args
            if(counter > 0){
                error_handler(3, "ls");
            }else{
                listDir();
            }
            break;
        case PWD:
            // 0 args
            if(counter > 0){
                error_handler(3, "pwd");
            }else{
                showCurrentDir();
            }
            break;
        case CP:
            // 2 args
            //printf("%s\n", tbuf[1]);
            if(counter != 2){
                error_handler(3, "cp");
            }else{
                copyFile(tbuf[0], tbuf[1]);
            }
            break;
        case MV:
            // 2 args
            if(counter != 2){
                error_handler(3, "mv");
            }else{
                moveFile(tbuf[0],tbuf[1]);
            }
            break;
        case MKDIR:
            // 1 args
            if(counter != 1){
                error_handler(3, "mkdir");
            }else{
                makeDir(tbuf[0]);
            }
            break;
        case CD:
            // 1 args
            if(counter != 1){
                error_handler(3, "cd");
            }else{
                changeDir(tbuf[0]);
            }
            break;
        case RM:
            // 1 args
            if(counter != 1){
                error_handler(3, "rm");
            }else{
                deleteFile(tbuf[0]);
            }
            break;
        case CAT:
            // 1 args
            if(counter != 1){
                error_handler(3, "cat");
            }else{
                displayFile(tbuf[0]);
            }
            break;
        default:
            // this case is for unrecognized command
            giveup = 1;
            error_handler(1, stoken);
            goto label1;
            break;
    }

    label1:
    free(tbuf);
    return giveup;
}


int main(int argc, char** argv){
    setbuf(stdout, NULL);
    setbuf(stdin, NULL);

    /* ---------- Variables ------------- */
    int f_mode = 0;    // flag for checking which mode is on.
    char* line = NULL;
    char* token = NULL;
    char* seveptr;
    //char* c_command;
    size_t len = 0;
    ssize_t nread;
    int condition = 1;

    /* ---------- Mode control ---------- */
    if(argc > 1){
        // argv: 0              1  2
        //       ./pseudo-shell -f input.txt
        if(argc > 3){
            // too many input files, then exit.
            write(1, argv[0], strlen(argv[0]));
            error_handler(6, argv[0]);
            exit(EXIT_FAILURE);
        }else if(strcmp(argv[1], "-f") != 0){
            // Mode option error.
            error_handler(4, argv[1]);
        }else if(freopen(argv[2], "r", stdin) == NULL){
            // Invalid input for a NULL file, then exit.
            write(1, argv[0], strlen(argv[0]));
            error_handler(5, argv[2]);
            exit(EXIT_FAILURE);
        }else{
            // file mode active!
            freopen("output.txt", "w", stdout);
            f_mode = 1;
        }
    }

    /* ---------- Main loop ------------- */

    while(condition){
        write(1, ">>> ", 4);
        if((nread = getline(&line, &len, stdin)) < 0) break;
        // eliminate "\n"
        line[strcspn(line, "\n")] = '\0';
        char* charbuf = line;
        // Delimit commands by control code first, then use handler delimit by blanks
        while((token = strtok_r(charbuf, ";", &seveptr)) != NULL){
            charbuf = NULL;
            // Since taking control code as AND operator(which is binary), only has the situation <commands>;<commands>.
            if(strcmp(token, " ") == 0){
                error_handler(1, " ");
                // error found, give up rest of line.
                break;
            }else if((strcmp(token, "exit ")==0)||(strcmp(token, "exit")==0)){
                condition = 0;
                break;
            }
            if(commands_handler(token)){
                // if detected an error stop processing the line.
                break;
            }
            
        }
    }

    /* ---------- free part ------------- */
    free(line);
    fclose(stdin);
    fclose(stdout);

    return 0;
}
