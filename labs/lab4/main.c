/* Lab4
 *
 */

#define _GUN_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(){
    pid_t pid;
    // make pid array for more process

    printf("Main start here my pid is %d\n", getpid());

    pid = fork();
    if (pid == 0){
        printf("This is the child process, my pid is %d, my parent pid is %d\n",getpid(), getppid());
        printf("My status is %d \n\n", pid);
        /*if(execv("./hello", NULL) < 0 ){
            // means not running
            perror("execv\n");
        }*/
        sleep(2);
    }else{
        wait(0);
        printf("Main existing, my pid is %d\n", getpid());
    }
}
