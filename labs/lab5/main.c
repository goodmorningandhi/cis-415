/* Lab5
 * Author: Liwei Fan
 */

//#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>

#define PPOOL 5

void sigHandler(int sig){

  printf("Child Process: %d - Received signal: SIGUSR1\n",getpid());
}

void signaler(pid_t *pid, int sig){
    for(int j = 0; j < PPOOL; j++){
        sleep(1);
        // Since out of Main loop, current pid is parent id,
        // child pid should be each loop pid
        printf("Parent process: %d - Sending signal: %d to child process: %d\n", getpid(), sig, pid[j]);
        kill(pid[j], sig);
    }
}

int main(){
    // variables
    sigset_t set;
    pid_t pid[PPOOL];

    // init set
    sigemptyset(&set);
    sigaddset(&set, SIGUSR1);
    struct sigaction act;
    act.sa_handler = sigHandler;
    act.sa_mask = set;
    act.sa_flags = 0;
    sigaction(SIGUSR1, &act, NULL);
    // main loop
    for(int i = 0; i < PPOOL; i++){
        pid[i] = fork();

        if(pid[i] < 0){
            // fork error
            fprintf(stderr, "Child Process: %d - Fork Failed\n", getpid());
            exit(-1);

        }else if(pid[i] == 0){
            // child process
            printf("Child Process: %d - Waiting for SIGUSR1...\n",getpid());
            int sig;
            sigwait(&set, &sig);
            printf("Child Process: %d - Received signal: SIGUSR1 - Calling exec().\n",getpid());
            if(execv("./iobound", NULL) < 0 ){
                // means not running
                perror("execv\n");
                sleep(1);
            }
            break;

        }else{
            // parent process here
            //printf("Main existing, my pid is %d\n", getpid());
            sleep(1);
        }
    }
    fflush(stdout);
    signaler(pid, SIGUSR1);
    signaler(pid, SIGINT);

    return 0;
}
