#include "command.h"

void lfcat()
{
	// Define your variables here
	char cwd[MAX_SIZE];
	DIR* d;
	struct dirent *dptr;
	// unreadable file list
	char* files[] = {".", "..", "main.c", "command.h", "command.c", "output.txt",
					"input.txt", "a.out", "log.txt", "main", "output1.txt",
					"Makefile", ".DS_Store"};
	//char* c_file;
	int flag = 1;
	char* lines = NULL;
	size_t len = 0;
	ssize_t read;


	// Get the current directory
	getcwd(cwd, sizeof(cwd));      // get current working dir


	// Open the dir using opendir()
	d = opendir(cwd);              // open

	// use a while loop to read the dir
	while(flag){
		int x = -1;
		// Hint: use an if statement to skip any names that are not readable files (e.g. ".", "..", "main.c", "a.out", "output.txt"

		if((dptr = readdir(d)) == NULL) break;


		for(int i = 0; i < 13; i++){
			if(strcmp((dptr->d_name), files[i]) == 0) x = i;
		}
		if(x >= 0) {
			continue; // ignore unreadable files
		}
			// Open the file
		freopen(dptr->d_name, "r", stdin);
			// Read in each line using getline()
				// Write the line to stdout
		write(1, "File: ", 6);
		int linelen = strlen(dptr->d_name);
		write(1, (dptr->d_name),linelen);
		write(1, "\n", 1);

		while ((read = getline(&lines, &len, stdin)) != -1){
			write(1, lines, strlen(lines));     // 1 is file discriptor to stdout
			//free(lines);
		}
		write(1, "\n", 1);
			// write 80 "-" characters to stdout
		for(int j = 0; j < 80; j++) write(1, "-", 1);
			// close the read file and free/null assign your line buffer
		write(1, "\n", 1);
		fclose(stdin);
	}
	//close the directory you were reading from using closedir()
	free(lines);
	free(dptr);
	closedir(d);
}
