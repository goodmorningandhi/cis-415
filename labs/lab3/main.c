/*
* Description: CIS415 Lab3.
*              Execute system calls(lfcat) in C.
*
* Author: Liwei Fan
*
* Date: Apr. 16th, 2020
*
* Notes:
* 1. lfcat() is a novel command that lists all files and their contents
*/

/*-------------------------Preprocessor Directives---------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>
#include "command.h"
/*---------------------------------------------------------------------------*/

/*-----------------------------Program Main----------------------------------*/
int main(int argc, char** argv) {
	setbuf(stdout, NULL);

	// Variable declarations
	int condition = 1;       // flag
	char* line = NULL;
	char* token = NULL;
	char* seveptr;
	int endline;
	size_t len = 0;
	ssize_t read;
	int flag = 0;
	//int counter = 0;
	/* Switch the context of STDOUT if the program is started with the -f flag.
	   Open the file for reading and open a file "output.txt" for writing.
	   Hint: use freopen() to switch the context of a file descriptor. */
	if((argc > 1)&&(strcmp(argv[1], "-f") == 0)){
		freopen("output.txt", "w", stdout);
		// Remove the newline at the end of the input string.
		endline = strcspn(argv[2], "\n");
		argv[2][endline] = '\0';
		if (freopen(argv[2], "r", stdin) == NULL){
			fclose(stdout);
			freopen("output.txt", "w", stderr);
			write(2, argv[0], strlen(argv[0]));
			write(2, ": Invalide input file", 21);
			fclose(stderr);
			exit(EXIT_FAILURE);
		}
	}
	 	//Hint: use strcspn(2) to find where the newline is then assign '\0' there.

	// Main run cycle
	do {

		// Display prompt and read input from console/file using getline(3)
		write(1,">>> ",4);
		read = getline(&line, &len, stdin);
		if(read == -1){
			break;
		}
		char *charbuf = line;
		/* Tokenize the input string */
		while((token = strtok_r(charbuf, " ", &seveptr)) != NULL){
			/* Display each token */
			charbuf = NULL;
			if(strcmp(token, "\n")==0) {
				continue;
			}
			endline = strcspn(token, "\n");
			token[endline] = '\0';
			if(strcmp(token, "exit") == 0){
				/* If the user entered <exit> then exit the loop */
				fprintf(stdout, "\n");
				condition = 0;
				break;
			}else if(strcmp(token, "lfcat") == 0){
				if((strcspn(seveptr,";")==0)||(strcspn(seveptr, "\0")==0)||(strcspn(seveptr, "\n")==0)){
					lfcat();
				}else {
					int posi = strcspn(seveptr, "\n");
					seveptr = seveptr+posi;
					write(1, "Error! Unsupported parameters for command: lfcat\n", 49);
				}
			}else{
				if(0 == strcmp(token, ";")){
					//do nothing.
				}else{
					write(1, "Error! Unrecognized command: ", 29);
					write(1, token, strlen(token));
					write(1, "\n", 1);
				}
			}
		}
		/* Tokenize and process the input string. Remember there can be multiple
		   calls to lfcat. i.e. lfcat ; lfcat <-- valid
		 	 If the command is either 'exit' or 'lfcat' then do the approp. things.
		   Note: you do not need to explicitly use an if-else clause here. For
			     instance, you could use a string-int map and a switch statement
					 or use a function that compares the input token to an array of
					 commands, etc. But anything that gets the job done gets full points so
					 think creatively. ^.^  Don't forget to display the error messages
					 seen in the lab discription*/

	} while(condition);

	/*Free the allocated memory and close any open files*/
	free(line);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
/*-----------------------------Program End-----------------------------------*/
