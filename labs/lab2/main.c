/*
* Description: lab2 takes complex input from the console on a line-by-line
*              basis and parsing that into understandable tokens that can be used by the system.
*
* Author: Liwei Fan
*
* Date: Apr. 9th 2020
*
* Notes:
* 1. This version is for console.
* 2. Using system auto malloc for lines and tokens.
*/

/*-------------------------Preprocessor Directives---------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*---------------------------------------------------------------------------*/

/*-----------------------------Program Main----------------------------------*/
int main() {
	setbuf(stdout, NULL);

	/* Main Function Variables */
	char *line = NULL;
	char *token = NULL;
	char *seveptr;
	size_t len = 0;
	ssize_t read;
	int flag = 1;
	int counter = 0;
	/* Allocate memory for the input buffer. */
	/*main run loop*/
	while(flag){
		
		/* Print >>> then get the input string */
		printf(">>> ");
		read = getline(&line, &len, stdin);
		char *charbuf = line;
		/* Tokenize the input string */
		while((token = strtok_r(charbuf, " ", &seveptr)) != NULL){
			/* Display each token */
			charbuf = NULL;
			if((strcmp(token, "exit\n") == 0)||(strcmp(token, "exit") == 0)){
				/* If the user entered <exit> then exit the loop */
				printf("\n");
				flag = 0;
				break;
			}else if(strcmp(token,"\n")==0){
				//printf("\n");
			}else{
				printf("\r\nT%d: %s", counter++, token);
			}
		}
	}
	/*Free the allocated memory*/

	free(line);

	return 0;
}
/*-----------------------------Program End-----------------------------------*/
