/*
* Description: lab2 takes complex input from the file on a line-by-line
*              basis and parsing that into understandable tokens that can be used by the system.
*
* Author: Liwei Fan
*
* Date: Apr. 9th 2020
*
* Notes:
* 1. This version is for file.
* 
*/

/*-------------------------Preprocessor Directives---------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*---------------------------------------------------------------------------*/

/*-----------------------------Program Main----------------------------------*/
int main(int argc, char** argv) {
	setbuf(stdout, NULL);

	/* Main Function Variables */
	FILE* fp1;						// For input file
	FILE* fp2;						// For output file
	char* line = NULL;
	char* token = NULL;
	char* seveptr;
	size_t len = 0;
	ssize_t read;
	int flag = 1;
	int counter = 0;
	if ((fp1 = fopen(argv[1],"r")) == NULL){
		fprintf(stderr, "%s: Invalide input file", argv[0]);
		exit(EXIT_FAILURE);
	}
	fp2 = fopen(argv[2], "w");

	/* Allocate memory for the input buffer. */
	/*main run loop*/
	while((read = getline(&line, &len, fp1)) != -1){

		/* Print >>> then get the input string */
		// For file version, no >>> will be output.
		char *charbuf = line;
		/* Tokenize the input string */
		while((token = strtok_r(charbuf, " ", &seveptr)) != NULL){
			/* Display each token */
			charbuf = NULL;
			if((strcmp(token, "exit\n") == 0)||(strcmp(token, "exit") == 0)){
				/* If the user entered <exit> then exit the loop */
				flag = 0;
				break;
			}else if(strcmp(token,"")==0){
				//printf("\n");
			}else{
				if(token[(strlen(token)-1)] == '\n'){
					fprintf(fp2, "T%d: %s", counter++, token);
				}else{
					fprintf(fp2, "T%d: %s\r\n", counter++, token);
				}
			}
		}
	}
	/*Free the allocated memory*/
	
	free(line);
	fclose(fp1);
	fclose(fp2);
	return 0;
}
/*-----------------------------Program End-----------------------------------*/
